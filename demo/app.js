(function app(window, angular) {

    var deps = ['ez.edit'];
    window.appName = 'ez.edit.demo';
    angular.module(window.appName, deps);

}(window, window.angular));
