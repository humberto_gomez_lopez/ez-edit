(function controller(app) {

    // Register the controller
    app.controller('DemoController', ['$scope', 'data', demoController]);

    /**
    * The controller for the demo page.
    * @param $scope The controller scope
    * @param data The demo data.
    */
    function demoController($scope, data) {
        $scope.data = data;
    }

}(window.angular.module(window.appName)));
