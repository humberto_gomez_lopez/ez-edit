(function data(app, storage) {

    // Company and name constants.
    var COMPANIES = ['Microsoft', 'Google', 'Yahoo', 'Mozilla', 'BaseTIS',
        'Apple', 'IBM', 'CocaCola', 'Pepsi', 'CNN', ''],
        NAMES = ['Mathew', 'Marc', 'Luke', 'John', 'Ceasar', 'Brutus',
            'Cleopatra', 'Florence', 'Emily', 'Robert', 'Mary'],
        KEY = 'DATA',
        COUNT = 20;

    app.factory('data', [demoDataServiceFactory]);

    /** Provides the data to be used on the demo page. */
    function demoDataServiceFactory() {
        var data;
        if (storage[KEY]) {
            data = JSON.parse(storage[KEY]);
        } else {
            data = generateData(COUNT);
            storage[KEY] = JSON.stringify(data);
        }
        return data;

        /** Generates the demo data */
        function generateData(count) {
            var res = [];
            for (var i = 0; i < count; i++) {
                res.push({
                    id: i,
                    name: NAMES[Math.floor(Math.random() * NAMES.length)],
                    company: COMPANIES[Math.floor(Math.random() * COMPANIES.length)],
                    age: 20 + Math.floor(Math.random() * 40)
                });
            }
            return res;
        }
    }

}(window.angular.module(window.appName), window.sessionStorage));
