(function plugins(app, $) {

    // Register for run
    app.run(['$compile', 'ezEdit', demoPlugins]);

    /** The plugins for the demo */
    function demoPlugins($compile, edit) {
        edit.add('text', {
            edit: editHandler,
            transforms: [{ view: upperCase }]
        });
        edit.add('number', {
            edit: editHandler,
            transforms: ['number']
        });

        /** Returns the value as upper case */
        function upperCase(element, scope, val) {
            return String(val).toUpperCase();
        }

        /** Called to put the element into edit mode. */
        function editHandler(scope, element, model) {
            var input, pos;

            // Prepare the element.
            element.empty();
            pos = element.css('position');
            if (pos === '' || pos === 'static') {
                element.css('position', 'relative');
            }

            // Setup the input
            input = $('<input type="text" />');

            // Set the model value
            input.attr('ng-model', model);
            element.append(input);

            // Process the editor through angular.
            $compile(element.contents())(scope);
        }
    }

}(window.angular.module(window.appName), window.angular.element));
