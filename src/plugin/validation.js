/**
* Provides the default system validations.
*/
(function validationPlugins(mod) {
    'use strict';
    // Register the factory
    mod.config(['ezEditValidationProvider', validationsConfig]);

    /**
    * Defines the default system validations.
    * @param {object} validation The validationProvider to add the
    *   validations to.
    */
    function validationsConfig(validation) {
        // Register the integrated validations
        validation.add('required', required);
        validation.add('minLength', minLength);
        validation.add('maxLength', maxLength);
        validation.add('minValue', minValue);
        validation.add('maxValue', maxValue);

        /**
        * Checks that the value is truthy (except for 0).
        * @param {object} The element the validation is assigned to.
        * @param {object} scope The scope the validation is taking place on.
        * @param value The value to check.
        * @returns {array} An array containing any errors that may have occured.
        */
        function required(element, scope, value) {
            return value || value === 0;
        }

        /**
        * Ensures the value is not longer than the value supplied in params.
        * @param {object} The element the validation is assigned to.
        * @param {object} scope The scope the validation is taking place on.
        * @param value The value to check.
        * @param oldValue The value that was defined before.
        * @param {array} params The parameters passed with the validation.
        * @returns {boolean} True if the length is less than the supplied value
        *   otherwise false.
        */
        function maxLength(element, scope, value, oldValue, params) {
            var max = parseInt(params[0], 10),
                errors = [];
            if (!isNaN(max)) {
                if (value !== undefined && value !== null) {
                    value = String(value);
                    if (value.length > max) {
                        return false;
                    }
                }
            }
            return true;
        }

        /**
        * Ensures the value is not shorter than the value supplied in params.
        * @param {object} The element the validation is assigned to.
        * @param {object} scope The scope the validation is taking place on.
        * @param value The value to check.
        * @param oldValue The value that was defined before.
        * @param {array} params The parameters passed with the validation.
        * @returns {boolean} True if the value length is less than the supplied
        *   value.
        */
        function minLength(element, scope, value, oldValue, params) {
            var min = parseInt(params[0], 10);
            if (!isNaN(min)) {
                if (value !== undefined && value !== null) {
                    value = String(value);
                    if (value.length < min) {
                        return false;
                    }
                }
            }
            return true;
        }

        /**
        * Ensures the value is not greater than the value supplied in params.
        * @param {object} The element the validation is assigned to.
        * @param {object} scope The scope the validation is taking place on.
        * @param value The value to check.
        * @param oldValue The value that was defined before.
        * @param {array} params The parameters passed with the validation.
        * @returns {boolean} True if the value is less than or equal to the
        *   supplied value, otherwise false.
        */
        function maxValue(element, scope, value, oldValue, params) {
            var max = parseFloat(params[0]);
            if (isNaN(max)) {
                //We allow string comparison, but by parsing first,
                // we ensure number comparison is done by default.
                // Note: This will cause issues where lexical (string)
                //  comparison is desired, and the supplied value is a number...
                //  eg. "10" < "2" and 10 > 2.
                max = params[0];
            }

            // If we have no value, return true (required can be used to force
            //  entry)
            if (value !== undefined && value !== null) {
                if (typeof max === 'string') {
                    value = String(value);
                } else {
                    value = parseFloat(value);
                }

                if (value > max) {
                    return false;
                }
            }
            return true;
        }

        /**
        * Ensures the value is not less than the value supplied in params.
        * @param {object} The element the validation is assigned to.
        * @param {object} scope The scope the validation is taking place on.
        * @param value The value to check.
        * @param oldValue The value that was defined before.
        * @param {array} params The parameters passed with the validation.
        * @returns {boolean} An array containing any errors that may have
        *   occured.
        */
        function minValue(element, scope, value, oldValue, params) {
            var min = parseFloat(params[0]);
            if (isNaN(min)) {
                //We allow string comparison, but by parsing first,
                // we ensure number comparison is done by default.
                // Note: This will cause issues where lexical (string)
                //  comparison is desired, and the supplied value is a number...
                //  eg. "10" < "2" and 10 > 2.
                min = params[0];
            }

            // If we have no value, return true (required can be used to force
            //  entry)
            if (value !== undefined && value !== null) {
                if (typeof max === 'string') {
                    value = String(value);
                } else {
                    value = parseFloat(value);
                }

                if (value < min) {
                    return false;
                }
            }
            return true;
        }

    }

}(window.angular.module('ez.edit')));
