/**
* @module TransformPlugins The transform plugins module allows global transforms
*   to be added and used.
*/
(function transformPlugins(mod) {
    'use strict';
    // Register the config
    mod.config(['ezEditTransformProvider', transformsConfig]);

    /**
    * Defines the default system transforms.
    * @param {object} transformProvider The transformProvider to add the
    *   validations to.
    */
    function transformsConfig(transform) {
        var lg;

        // Adds the number
        transform.add('number', toString, toNumber);
        transform.add('date', dateToString, toDate);

        /** Provides late injection of the $log service */
        function log() {
            if (!lg) {
                lg = angular.injector(['ng']).get('$log');
            }
            return lg;
        }

        /** Returns the string representation of the value */
        function toString(element, scope, val) {
            return String(val);
        }

        /** Calls parseFloat on the given value. */
        function toNumber(element, scope, val) {
            return parseFloat(val);
        }

        /** Converts the number or string value to a date. */
        function toDate(element, scope, val) {
            var res;
            if (typeof val === 'string' || typeof val === 'number') {
                res = Date.parse(val);
                if (isNaN(res)) {
                    log().warn('Unabke to convert the supplied value (' + val +
                        ') to a date.');
                    res = val;
                }
            } else if (val instanceof Date) {
                res = val;
            } else {
                log().warn('The supplied value is not a number, string or ' +
                    'date. Unable to return a date');
                res = val;
            }
            return res;
        }

        /**
        * Converts a date object to a string.
        * @param val The date value to conevrt to the string.
        * @returns The ISO string representing the supplied date.
        */
        function dateToString(element, scope, val) {
            if (val instanceof Date) {
                return val.toISOString();
            } else {
                return val;
            }
        }

    }

}(window.angular.module('ez.edit')));
