/**
* The main extry point to the application. This registers the module with
*   angular.
*/
(function exEditModule(angular) {
    'use strict';
     // Register the module.
     angular.module('ez.edit', []);
}(window.angular));
