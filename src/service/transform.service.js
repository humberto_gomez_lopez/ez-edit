/**
* This module defines the ezEditTransformProvider which allows global
*   validations to be defined.
*/
(function ezEditTransformModule(mod) {
    'use strict';

    // Register the factory
    mod.provider('ezEditTransform', [ezEditTransformProviderFactory]);

    /**
    * Creates the ezEditTransform provider. Note: The provider and the
    *   factory are the same object.
    */
    function ezEditTransformProviderFactory() {
        var provider = { },
            transforms = { };

        // Assign the public API
        provider.$get = $get;
        provider.transformEdit = transformEdit;
        provider.transformView = transformView;
        provider.add = addTransform;
        provider.remove = removeTransform;
        provider.transform = transform;

        return provider;

        /** Returns the service (which is also the service) */
        function $get() {
            return provider;
        }

        /**
        * Returns the transform with the given name.
        * @param {string} name The name of the transform to retrieve.
        * @return The transform object.
        */
        function transform(name) {
            return transforms[name];
        }

        /**
        * Performs the named view transform or transforms (in order).
        * @param {string | array} name The name of the transforms, or an array
        *   of transforms to perform.
        * @param {object} element The element the transform is being performed
        *   on.
        * @param {object} scope The scope the transform is happening on.
        * @param value The value to transform.
        * @returns The transformed value.
        */
        function transformView(name, element, scope, value) {
            if (transforms.hasOwnProperty(name)) {
                return transforms[name].view(element, scope, value);
            } else {
                throw new Error('Transform named "' + name + '" does not exist');
            }
        }

        /**
        * Performs the named edit transform or transforms (in order).
        * @param {string | array} name The name of the transforms, or an array
        *   of transforms to perform.
        * @param {object} element The element the transform is being performed
        *   on.
        * @param {object} scope The scope the transform is happening on.
        * @param value The value to transform.
        * @returns The transformed value.
        */
        function transformEdit(name, element, scope, value) {
            if (transforms.hasOwnProperty(name)) {
                return transforms[name].edit(element, scope, value);
            } else {
                throw new Error('Transform named "' + name + '" does not exist');
            }
        }

        /**
        * Adds a transform handler.
        * @param {string} name The name of the transform. Used by plugins to
        *   transform the values.
        * @param {function} view The handler to use when converting from edit
        *   value to a string.
        * @param {function} The function to use to convert from a string to the
        *   edit value.
        */
        function addTransform(name, view, edit) {
            if (typeof name !== 'string') {
                throw new Error('name MUST be a string');
            }
            if (typeof view !== 'function') {
                throw new Error('view MUST be a function');
            }
            if (typeof edit !== 'function') {
                throw new Error('edit MUST be a function');
            }
            transforms[name] = {
                view: view,
                edit: edit
            };
        }

        /**
        * Removes an existing transform.
        * @param {string} name The name of the transform to remove.
        * @returns {object} The removed transform.
        */
        function removeTransform(name) {
            var res = transforms[name];
            delete transforms[name];
            return res;
        }
    }

}(window.angular.module('ez.edit')));
