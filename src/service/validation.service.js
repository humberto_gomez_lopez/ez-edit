/**
* This module defines the ezEditValidationProvider which allows global
*   validations to be defined.
*/
(function ezEditValidationModule(mod) {
    'use strict';
    // Register the factory
    mod.provider('ezEditValidation', [ezEditValidationProviderFactory]);

    /**
    * Creates the ezEditValidation provider. Note: The provider and the
    *   factory are the same object.
    */
    function ezEditValidationProviderFactory() {
        var provider = { },
            validations = { };

        // Assign the public API
        provider.$get = $get;
        provider.validate = validate;
        provider.validation = validation;
        provider.add = addValidation;
        provider.remove = removeValidation;

        return provider;

        /** Returns the service (which is also the service) */
        function $get() {
            return provider;
        }

        /**
        * Returns the named validation.
        * @param {string} name The name of the validation to return.
        */
        function validation(name) {
            return validations[name];
        }

        /**
        * Performs the named validation or validations.
        * @param {string | array} name The name of the validation, or an array
        *   of validations to perform.
        * @param {object} element The element the validation is being performed
        *   on.
        * @param {object} scope The scope the validation is happening on.
        * @param value The new value to validate.
        * @param oldValue The original value.
        * @param {array} params The parameters passed to the validation
        *   from the view.
        */
        function validate(name, element, scope, value, oldValue, params) {
            if (validations.hasOwnProperty(name)) {
                return validations[name](element, scope, value, oldValue,
                    params);
            } else {
                throw new Error('validation named "' + name +
                    '" does not exist.');
            }
        }

        /**
        * Adds a validation handler.
        * @param {string} name The name of the validation. Used later in the
        *   view to apply validations to various fields. If the validation
        *   already exists it will be replaced.
        * @param {function} check The handler to use to apply the validation.
        *   This function should have the following signature:
        *     (element, scope, value, oldValue) returns Array containing errors
        */
        function addValidation(name, check) {
            if (typeof name !== 'string') {
                throw new Error('name MUST be a string');
            }
            if (typeof check !== 'function') {
                throw new Error('check MUST be a function');
            }
            validations[name] = check;
        }

        /**
        * Removes an existing validation.
        * @param {string} name The name of the validation to remove.
        * @returns The validation that was removed.
        */
        function removeValidation(name) {
            var res;
            if (validations.hasOwnProperty(name)) {
                res = validations[name];
                delete validations[name];
            }
            return res;
        }
    }

}(window.angular.module('ez.edit')));
