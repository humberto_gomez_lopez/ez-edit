/**
* This module defines the easy edit service. This service is responsible for
*   registering the main ez-edit service which can be used to configure
*   providers, and also provides the main code for working with the plugins.
*/
(function ezEditService(mod) {
    'use strict';
    // Register the factory
    mod.provider('ezEdit', ['ezEditTransformProvider', 'ezEditValidationProvider',
        ezEditProviderFactory]);

    /**
    * Creates and returns the ez edit provider.
    */
    function ezEditProviderFactory(transform, validation) {
        var plugins = { };

        // Return the provider.
        return {
            addPlugin: addPlugin,
            removePlugin: removePlugin,
            list: list,
            plugin: plugin,
            $get: ezEditServiceFactory
        };

        /**
        * Adds a new plugin to the system.
        * @param {string} type The type the plugin will handle.
        *   Note: If a plugin with the same name exists, it will be overwritten.
        * @param {object} The plugin to use to handle the editing.
        *   The plugin object should have the following properties.
        *       * edit(scope, element, model) - The function to call to replace
        *           the contents of the element with the editor.
        *       *   * scope - The scope of the element.
        *       *   * element - The element the directive is applied to.
        *       *   * model - A string expression containing the data binding
        *       *   *   value on the scope.
        *       * view(scope, element, value) - Optional function to call to
        *           replace the contents of the element with the value.
        *       * transforms - Optional array of transform names.
        *       * validations - Optional validations to perform.
        */
        function addPlugin(type, plugin) {
            var i;
            if (typeof type !== 'string') {
                throw new Error('type MUST be a string');
            }
            validatePlugin(plugin);
            plugin.transforms = plugin.transforms || [];
            plugin.validations = plugin.validations || {};
            plugins[type] = plugin;

            for (i = 0; i < plugin.transforms.length; i++) {
                if (typeof plugin.transforms[i] === 'string') {
                    plugin.transforms[i] = transform.transform(plugin.transforms[i]);
                }
            }
        }

        /**
        * Removes a plugin from the system.
        * @param {string} type The type of plugin to remove.
        */
        function removePlugin(type) {
            var res = plugins[type];
            delete plugins[type];
            return res;
        }

        /**
        * Lists the types of all registered plugins.
        */
        function list() {
            return Object.keys(plugins);
        }

        /**
        * Returns the plugin for the given type. If no plugin exists, returns
        *   undefined.
        * @param {string} type The type of plugin to search for.
        */
        function plugin(type) {
            if (plugins.hasOwnProperty(type)) {
                return plugins[type];
            } else {
                throw new Error('No plugin named "' + type + '"');
            }
        }

        /**
        * Ensures that the supplied plugin contains all required functions.
        * @param {object} plugin The plugin to validate.
        * @throws {Error} An error if plugin is invalid.
        */
        function validatePlugin(plugin) {
            if (typeof plugin !== 'object') {
                throw new Error('plugin MUST be an object');
            }
            if (typeof plugin.edit !== 'function') {
                throw new Error('plugin.edit MUST be a function');
            }
            if (plugin.view && typeof plugin.view !== 'function') {
                throw new Error('When supplied, plugin.view MUST be a function');
            }
            if (plugin.transforms && !Array.isArray(plugin.transforms)) {
                throw new Error('When supplied, plugin.transforms MUST be ' +
                    'an array');
            }
            if (plugin.validations && typeof plugin.validations !== 'object') {
                throw new Error('When supplied, plugin.validations MUST be ' +
                    'an object');
            }
        }

        /** Creates the ez edit service. */
        function ezEditServiceFactory() {
            return {
                add: addPlugin,
                remove: removePlugin,
                list: list,
                plugin: plugin
            };
        }

    }

}(window.angular.module('ez.edit')));
