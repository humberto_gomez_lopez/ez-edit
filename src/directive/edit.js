/**
* @module EditModule The edit module is used to defined the ez-edit directive.
*   This directive can be used to provide edit plugins inside of templates.
*   It should be used in the form:
*       <div ez-edit='<plugin name>' ng-model='<scope location>'></div>
*/
(function editModule(mod) {
    'use strict';

    // Constants
    // The regex to extract the parameter parts with.
    var PARAMEX = /^([^\[\]]*)(?:$|(\[.*\])$)/;

    // Register the directive
    mod.directive('ezEdit', ['$log', '$timeout', '$parse', 'ezEdit',
        'ezEditValidation', 'ezEditTransform', editDirective]);

    /**
    * Defines the edit directive.
    * @param $log Used to print a warning if no plugin type is supplied.
    * @param $timeout Used to delay execution to the following render cycle.
    * @param $parse Used to parse attributes.
    * @param edit The edit service. Used to access plugins.
    * @param validationService Used to access validations.
    * @param transformService Used to access transforms.
    */
    function editDirective($log, $timeout, $parse, edit, validationService,
        transformService) {
        return {
            restrict: 'A',
            scope: true, // Ensure the element has a scope.
            // ng-model required since we use that to manage the state
            require: ['ngModel'],
            link: link
        };

        /** Performs the directive link up. */
        function link(scope, element, attrs, reqs) {
            var type = attrs.ezEdit,
                editing = false,
                eleValidations,
                original,
                ngModel,
                plugin,
                modval,
                tval,
                val,
                i;

            if (type) {
                ngModel = reqs[0];
                plugin = edit.plugin(type);

                // Transforms
                modval = $parse(attrs.ngModel)(scope);
                tval = modval;
                ngModel.$modelValue = val;
                for (i = 0; i < plugin.transforms.length; i++) {
                    val = createFormatter(plugin.transforms[i]);
                    ngModel.$formatters.push(val);
                    tval = val(tval);
                    val = createParser(plugin.transforms[i]);
                    ngModel.$parsers.push(val);
                }

                // Plugin Validations
                for (i in plugin.validations) {
                    if (plugin.validations.hasOwnProperty(i)) {
                        val = createValidation(plugin.validations[i], []);
                        ngModel.$validators[i] = val;
                    }
                }

                // Element transforms and validations
                attrs.$observe('ezValidate', onValidationChanged);

                // Set the initial text
                if (plugin.view) {
                    plugin.view(scope, element, tval);
                } else {
                    element.text(tval);
                }

                // Prepare the scope
                scope.$eze = {
                    $$current: modval,
                    editing: isEditing,
                    edit: elementEdit,
                    cancel: elementCancel,
                    save: elementSave,
                    reset: elementReset,
                    valid: isValid,
                    errors: errors,
                    ngModel: ngModel,
                    element: element
                };

                scope.$watch('$eze.$$current', function (val) {
                    val = ngModel.$parsers.reduce(
                        parser,
                        val);
                    ngModel.$setViewValue(val);
                });

                // Emit the scope (after render) so we can access this element
                //   from a group without searching.
                $timeout(scope.$emit.bind(scope, 'ez.edit.added', scope));
                scope.$on('$destroy', onDestroyed);
            } else {
                $log.warn('ez-edit attribute is expected to have a valid ' +
                    'plugin name ("' + type + '" not found!)');
            }

            /**
            *   Triggers the given attribute (which is expected to contain an
            *     expression)
            *   @param {string} name The name of the attribute to trigger.
            */
            function trigger(name) {
                var func;
                if (attrs[name]) {
                    func = $parse(attrs[name]);
                    $timeout(execute);
                }

                /**
                * Attempts to parse and execute the contents of the
                * attribute.
                */
                function execute() {
                    func(scope);
                }
            }

            /** Creates a formatter function for the given transform */
            function createFormatter(transform) {
                return function(value) {
                    if (transform.view) {
                        return transform.view(element, scope, value);
                    } else {
                        return value;
                    }
                };
            }

            /** Creates a parser function for the given transform */
            function createParser(transform) {
                return function(value) {
                    if (transform.edit) {
                        return transform.edit(element, scope, value);
                    } else {
                        return value;
                    }
                };
            }

            /** Creates a validation function */
            function createValidation(validation, params) {
                if (typeof validation === 'string') {
                    validation = validationService.validate
                        .bind(validationService, validation);
                }
                res.validationName = validation;
                return res;

                function res(modelValue, viewValue) {
                    return validation(element, scope, modelValue, viewValue,
                        params);
                }
            }

            /** Returns the validation errors object */
            function errors() {
                return ngModel.$error;
            }

            /** Returns true when editing, else false */
            function isEditing() {
                return editing;
            }

            /** Calls validate, and checks that there are no errors. */
            function isValid() {
                ngModel.$validate();
                return Object.keys(ngModel.$error).length === 0;
            }

            /**
            * Sets the mode to editing or viewing according to the supplied
            *   param.
            */
            function setEditing(val) {
                if (editing !== val) {
                    editing = val;
                    if (val) {
                        scope.$parent.$emit('ez.edit.editing', scope);
                    } else {
                        scope.$parent.$emit('ez.edit.viewing', scope);
                    }
                }
            }

            /** Begins editing on the given element. */
            function elementEdit() {
                var type, plugin, ngmod;
                type = element.attr('ez-edit');
                ngmod = element.attr('ng-model');
                plugin = edit.plugin(type);
                scope.$eze.$$current = $parse(ngmod)(scope);
                plugin.edit(scope, element, '$eze.$$current');
                original = ngModel.$modelValue;
                setEditing(true);
                trigger('onedit');
            }

            /** Called to complete editing */
            function elementSave() {
                ngModel.$validate();

                // Check for errors.
                if (Object.keys(ngModel.$error).length === 0) {
                    // Ensure the model is updated.
                    ngModel.$commitViewValue();

                    // Go to view mode.
                    elementView();
                    trigger('onsave');
                    return true;
                } else {
                    trigger('onerror');
                    return false;
                }
            }

            /** Called to cancel editing without saving the value. */
            function elementCancel() {
                var tval = ngModel.$formatters.reduceRight(format, original);
                ssv(original);
                elementView();
                trigger('oncancel');
            }

            /**
            * Called to revert the value back to the original without
            *  ending edit mode.
            */
            function elementReset() {
                // Reset back to the original
                ssv(original);
                trigger('onreset');
            }

            /** Puts the element into view mode */
            function elementView() {
                var type, plugi, tval;
                type = element.attr('ez-edit');
                plugin = edit.plugin(type);
                original = undefined;
                tval = ngModel.$formatters.reduceRight(
                    format,
                    ngModel.$modelValue);
                if (plugin.view) {
                    plugin.view(scope, element, tval);
                } else {
                    element.text(tval);
                }

                setEditing(false);
                trigger('onview');
            }

            function ssv(val) {
                scope.$eze.$$current = val;
                val = ngModel.$parsers.reduce(parser, val);
                ngModel.$setViewValue(val);
            }

            /**
            * Reads any user defined validations from the supplied string.
            * @param {string} str The validations string.
            */
            function readValidations(str) {
                var params;
                // First get the individual parameters to parse
                params = readParamStrings(str);
                // Parse the params.
                params = params.map(parseParam);
                return params;

                /**
                * Uses the parameters regex to extract the parts and parse.
                */
                function parseParam(p) {
                    var parts = PARAMEX.exec(p);
                    return {
                        type: parts[1],
                        params: $parse(parts[2])(scope) || []
                    };
                }

                /** Splits the given string by level 0 comma. */
                function readParamStrings(str) {
                    var curr = 0,
                        pos = 0,
                        res = [],
                        level = 0,
                        ch;

                    while (curr < str.length) {
                        ch = str.charAt(curr);
                        switch (ch) {
                            case '[':
                                level++;
                                break;
                            case ']':
                                level--;
                                break;
                            case ',':
                                if (level === 0) {
                                    res.push(str.substr(pos, curr));
                                    pos = curr + 1;
                                }
                                break;
                            default:
                                break;
                        }
                        curr++;
                    }

                    // Add the last
                    if (curr > pos) {
                        res.push(str.substr(pos, curr));
                    }

                    return res;
                }
            }

            /** Used to run the format functions */
            function format(val, formatter) {
                return formatter(val);
            }

            /** Runs the parser on the value */
            function parser(val, p) {
                return p(val);
            }

            // ----------- EVENT HANDLERS -----------

            /**
            * Called when the scope has been destroyed. This allows any
            *   parent groups to know that this scope no longer exists
            *   and should not be used when determining state.
            */
            function onDestroyed() {
                scope.$parent.$emit('ez.edit.removed', scope);
            }

            /**
            * Called when the validations attribute has been changed. This is
            *   implemented in this fashion so that the ez-validations attribute
            *   may be implemented with an interpolation, and updated
            *   accordingly.
            */
            function onValidationChanged() {
                var existing, name, i;
                // Get all existing validations and clear them.
                existing = Object.keys(ngModel.$validators)
                    .filter(customValidation);
                for (i = 0; i < existing.length; i++) {
                    // Remove the existing validator.
                    delete ngModel.$validators[existing[i]];
                }

                // Add validations defined in the attribute.
                eleValidations = readValidations(element.attr('ez-validate'));
                for (i = 0; i < eleValidations.length; i++) {
                    name = eleValidations[i].type;
                    val = createValidation(name, eleValidations[i].params);
                    ngModel.$validators[name] = val;
                }

                /** Returns true if the item is a custom validation function. */
                function customValidation(name) {
                    return ngModel.$validators[name] &&
                        ngModel.$validators[name].validationName &&
                        // Not directly from the plugin
                        !plugin.validations[name];
                }
            }

            // --------- END EVENT HANDLERS ---------
        }
    }

}(window.angular.module('ez.edit')));
