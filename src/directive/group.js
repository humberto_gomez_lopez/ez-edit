/**
* @module GroupModule The group module allows multiple fields to be grouped
*   and accessed easily. Because of the scope inheritance model of angular,
*   this allows the edit functions to be easily accessed (through scope.$eze)
*/
(function editGroupModule(mod) {
    'use strict';
    mod.directive('ezEditGroup', ['$log', '$parse', '$timeout', editGroupDirective]);

    /** Defines the edit directive */
    function editGroupDirective($log, $parse, $timeout) {
        return {
            restrict: 'A',
            scope: true,
            link: link
        };

        /** Performs the directive link up. */
        function link(scope, element, attrs) {
            var editCount = 0,
                childCount = 0,
                children = { };

            scope.$eze = {
                editing: editing,
                edit: edit,
                cancel: cancel,
                save: save,
                reset: reset,
                valid: valid,
                errors: errors,
                element: element
            };

            // Handle sub items.
            scope.$on('ez.edit.added', onAdded);
            scope.$on('ez.edit.removed', onRemoved);

            // Emit the scope so we can access this element from a group
            //  without searching.
            scope.$parent.$emit('ez.edit.added', scope);
            scope.$on('$destroy', onDestroyed);
            scope.$on('ez.edit.editing', onEditing);
            scope.$on('ez.edit.viewing', onViewing);

            /**
            * Checks whether all child fields are busy editing.
            * @returns true if all child fields are editing, false if no child
            *   is editing, undefined if some children are editing.
            */
            function editing() {
                if (editCount === 0) {
                    // None are editing.
                    return false;
                } else if (editCount === childCount) {
                    // All are editing
                    return true;
                } else {
                    // Some are editing.
                    return undefined;
                }
            }

            /** Returns true if all children are valid, else false */
            function valid() {
                return Object.keys(children)
                    .map(val)
                    .reduce(isTrue, true);

                /**
                * Returns true if the child is valid, else false.
                * @param id The child id the validate.
                */
                function val(id) {
                    return children[id].$eze.valid();
                }
            }

            /** Returns an array of errors for the children in the group. */
            function errors() {
                return Object
                    .keys(children)
                    .map(errs);

                /**
                * Returns the child's errors, which may be an object (for
                *   fields), or an array (for groups).
                * @param id The child id to get the errors for.
                */
                function errs(id) {
                    return {
                        scope: children[id].$eze.scope,
                        element: children[id].$eze.element,
                        errors: children[id].$eze.errors()
                    };
                }

            }

            /** The reduction to truth function. */
            function isTrue(a, b) {
                return a && b;
            }

            /** Called when the scope has been destroyed */
            function onDestroyed() {
                scope.$parent.$emit('ez.edit.removed', scope);
            }

            /** Called when a child field begins editing. */
            function onEditing() {
                editCount++;
            }

            /** Called when a child field begins viewing. */
            function onViewing() {
                editCount--;
            }

            /** Called when a child element has been added */
            function onAdded(evt, sc) {
                evt.stopPropagation();
                children[sc.$id] = sc;
                if (scope.$eze.editing()) {
                    editCount++;
                }
                childCount++;
            }

            /** Called when a child element has been removed. */
            function onRemoved(evt, scope) {
                evt.stopPropagation();
                delete children[scope.$id];
                if (scope.$eze.editing()) {
                    editCount--;
                }
                childCount--;
            }

            /** Called to call edit on all children. */
            function edit() {
                Object.keys(children)
                    .map(e);
                if (Object.keys(children).length) {
                    scope.$parent.$emit('ez.edit.editing', scope);
                    trigger('onedit');
                }
                /** Executes the edit function on the field. */
                function e(id) {
                    return children[id].$eze.edit();
                }
            }

            /** Called to call cancel on all children. */
            function cancel() {
                Object.keys(children)
                    .forEach(c);
                if (Object.keys(children).length) {
                    scope.$parent.$emit('ez.edit.viewing', scope);
                    trigger('oncancel');
                    trigger('onview');
                }

                /** Calls cancel on the given child */
                function c(id) {
                    children[id].$eze.cancel();
                }
            }

            /** Called to call reset on all children. */
            function reset() {
                Object.keys(children)
                    .forEach(r);
                if (Object.keys(children).length) {
                    trigger('onreset');
                }

                /** Calls cancel on the given child */
                function r(id) {
                    children[id].$eze.reset();
                }
            }

            /** Called to call save on all children. */
            function save() {
                var res;
                if (!valid()) {
                    return false;
                }

                // Call save on all children
                res = Object.keys(children)
                    .map(s)
                    .reduce(isTrue, true);
                if (res && Object.keys(children).length) {
                    scope.$parent.$emit('ez.edit.viewing', scope);
                    trigger('onsave');
                    trigger('onview');
                }
                return res;

                /** Executes the edit function on the field. */
                function s(id) {
                    return children[id].$eze.save();
                }
            }

            /**
            *   Triggers the given attribute (which is expected to contain an
            *     expression)
            *   @param {string} name The name of the attribute to trigger.
            */
            function trigger(name, arg) {
                var func;
                if (attrs[name]) {
                    func = $parse(attrs[name]);
                    $timeout(execute);
                }

                /**
                * Attempts to parse and execute the contents of the
                * attribute.
                */
                function execute() {
                    func(scope, arg);
                }
            }
        }
    }

}(window.angular.module('ez.edit')));
