# Plugin creation walkthrough
This document guides you through the steps to creating a plugin, and the
available options.

## Preparation
Plugins are added / removed through the `ezEdit` service (Available in both
config and runtime phases.) Plugins can be registered by injecting the `ezEdit`
service, and the calling the `add` function, passing the type to register (or
replace). This plugin will then be used when the `ez-edit` attribute is set to
the defined type.

The following options are available to the plugins:

* `edit` - The function to call to enter edit mode. This function should construct
and prepare the element for edit mode. This function is required.

    `edit(scope, element, model)`.
    * `scope` - The scope of the element the `ez-edit` attribute is defined on
    * `element` - The element the `ez-edit` attribute is defined on.
    * `model` - The model location changes should be bound to (eg. This can
        be set as the `ng-model` attribute on any generated form control)

* `view` - The function to call to enter view mode. This function should change
the element to provide a view of the supplied value.

    `view(scope, element, value)`.
    * `scope` - The scope of the element the `ez-edit` attribute is defined on
    * `element` - The element the `ez-edit` attribute is defined on.
    * `value` - The model value to be displayed (Or transformed, then displayed).
        Note: This value will have had the view transforms run on it already.

* `transforms` - An array of objects defining the transforms to be added to
`ngModel` `$parsers` and `$formatters`. Note, the functions defined here expect
`(element, scope, value)`, instead of value only as per usual angular
`$formatters` and `$parsers`, and the additional arguments will be supplied by
the framework.
Each object should have 2 functions:
    * `view` - The function to add to $formatters.
    * `edit` - The function to add to $parsers.
* `validations` - An object containing items to add to the `ngModel.$validators`
object that will be used to validate the entries supplied.











s
