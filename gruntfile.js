module.exports = function(grunt) {

    init();
    registerTasks();

    function init() {
        grunt.initConfig({
            jshint: {
                options: {
                    strict: true
                },
                all: ['src/**/*.js']
            },
            uglify: {
                options: { mangle: true },
                build: {
                    files: {
                        'build/ez.edit.min.js': [
                            'src/module.js',

                            'src/service/transform.service.js',
                            'src/service/validation.service.js',
                            'src/service/edit.service.js',

                            'src/directive/edit.js',
                            'src/directive/group.js',

                            'src/plugin/transform.js',
                            'src/plugin/validation.js'
                        ]
                    }
                }
            }
        });
    }

    /** Defines the tasks */
    function registerTasks() {
        grunt.loadNpmTasks('grunt-contrib-jshint');
        grunt.loadNpmTasks('grunt-contrib-uglify');
        grunt.registerTask('default', ['jshint:all', 'uglify:build']);
    }

};
