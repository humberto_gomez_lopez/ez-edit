# EZ Edit Core
The EZ Edit core project provides a framework for allowing editing of values
inside the scope. In order to utilise the editing functionality, an ancillary
project is required to provide the various edit plugins.
See `ez-edit-angular-bootstrap` for an ancillary project example.

## Demo
Run `npm start` to launch a static server with the contents. Click the `demo`
directory to see the demo.

## Plugins
See the [plugin walkthrough](plugin.walkthrough.md) for the guide to creating
plugins.

## Usage
This project attempts to integrate as far as possible with `ngModel` in order
to remain familiar to users. The following is an example of a generated table
with edit capabilities (Note: The plugin controls here are for example and
do not exist in this project. The controls in the `ez-edit-angular-bootstrap`
project follow this example).

    <script src='build/e.edit.min.js'></script>

    <tr ng-repeat='user in users' ez-edit-group onsave='saveUser(user)'>
        <td>{{user.id}}</td>
        <td ez-edit='text' ng-model='user.name' ez-validate='required,minLength[2]'></td>
        <td ez-edit='select' ng-model='user.country' ez-ng-options='item.code as item.name for item in countries'></td>
        <td>
            <button ng-show='!$eze.editing()' ng-click='$eze.edit()'>Edit</button>
            <button ng-show='$eze.editing()' ng-click='$eze.save()'>Save</button>
            <button ng-show='$eze.editing()' ng-click='$eze.reset()'>Reset</button>
            <button ng-show='$eze.editing()' ng-click='$eze.cancel()'>Cancel</button>
        </td>
    </tr>

As you can see, the idea is to add a little to existing angular directives to
give the edit functionality without interfering with natural view design.

## Built in Validations and transforms
### Validations
* `required` - Ensures a value has been entered.
* `minLength[x]` - Checks the string has a minimum length of `x`.
* `maxLength[x]` - Checks the string has a maximum length of `x`.
* `minValue[x]` - Checks the value is not less than `x`.
* `maxValue[x]` - Checks the value is not greater than `y`.

### Transforms
* `number` - Converts between a float and a string.
* `date` - Converts between a date and a string.

## Events
The following event attributes are available on the elements where the directives
are applied.
* `onedit` - Executed when the field or group enters edit mode.
* `onview` - Executed when the field or group enters view mode.
* `oncancel` - Executed when the field or group edit has been cancelled.
* `onreset` - Executed when the field or group has been reset.
* `onsave` - Executed when the field or group is saved.

## API
Both edit and group attributes have the same API, and so are described together
below. In both cases the API is attached to the scope on the `$eze` property.

* `editing()` - Returns true if all children are editing, undefined if some
are editing, and false if none are editing.
* `valid()` - Returns true if all children are valid, else false.
* `edit()` - Puts all children into edit mode.
* `reset()` - Resets all children to their value at the start of editing.
* `save()` - Attempts to save all values. Will not save if invalid. Returns true
if saved, else false.
* `cancel()` - Cancels editing on all children.
* `errors()` - On fields, returns an object containing all validation errors. On
groups, returns an array of errors returned from all children.
* `element` - The element the directive has been applied to.
