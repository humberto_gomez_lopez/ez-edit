(function validationServiceSpec() {
    'use strict';

    describe('validation service', function () {
        var service;
        beforeEach(inject(['ezEditValidation', function (editValidation) {
            service = editValidation;
        }]));

        describe('$get()', function () {
            it('should exist', function () {
                expect(service.$get).toEqual(jasmine.any(Function));
            });
            it('should return the service', function () {
                expect(service.$get()).toBe(service);
            });
        });
        describe('validate(name, element, scope, value, oldValue, params)', function () {
            it('should exist', function () {
                expect(service.validate).toEqual(jasmine.any(Function));
            });
            it('should throw an error if the validation does not exist', function () {
                expect(service.validate.bind(service, 'foo bar'))
                    .toThrowError(/exist/);
            });
            it('should run the named validation function', function () {
                var run = false;
                service.add('test', check);
                service.validate('test');
                expect(run).toBe(true);

                function check() {
                    run = true;
                }
            });
        });
        describe('add(name, check)', function () {
            it('should exist', function () {
                expect(service.add).toEqual(jasmine.any(Function));
            });
            it('should ensure name is a string', function () {
                expect(service.add.bind(service, 1, noop))
                    .toThrowError(/name.*string/);
            });
            it('should ensure check is a function', function () {
                expect(service.add.bind(service, 'foo', 'bar'))
                    .toThrowError(/check.*function/);
            });
            it('should add the named validation', function () {
                var c;
                service.add('test', check);
                c = service.validation('test');
                expect(c).toBe(check);
                function check() { }
            });
        });
        describe('remove(name)', function () {
            it('should exist', function () {
                expect(service.remove).toEqual(jasmine.any(Function));
            });
            it('should remove the named validation', function () {
                service.add('test', noop);
                expect(service.validation('test')).toEqual(jasmine.any(Function));
                service.remove('test');
                expect(service.validation('test')).toBe(undefined);
            });
            it('should return the removed validation check', function () {
                var c;
                service.add('test', noop);
                c = service.validation('test');
                expect(service.remove('test')).toBe(c);
                expect(service.remove('test')).toBe(undefined);
            });
        });
    });

    function noop() { }
}());
