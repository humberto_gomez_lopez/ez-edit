(function editDirectiveTests() {
    'use strict';

    describe('ezEdit', function () {
        var $provide, $compile, $timeout, $rootScope, $scope, service,
            element, plugin, $log;

        beforeEach(module(['$provide', function (provide) {
            $provide = provide;
        }]));

        // Inject everything we need
        beforeEach(inject(['$compile', '$rootScope', '$timeout', 'ezEdit', function (compile, rootScope, timeout, edit) {
            $compile = compile;
            $rootScope = rootScope;
            $timeout = timeout;
            service = edit;

            $log = {
                _warn: 0,
                warn: function () {
                    this._warn++;
                }
            };
            $provide.value('$log', $log);
        }]));

        // Prepare the element
        beforeEach(function () {
            plugin = {
                _view: 0,
                _edit: 0,
                _tview: 0,
                _tedit: 0,
                _validation: 0,
                view: createCounter('view'),
                edit: createCounter('edit'),
                transforms: [{
                    view: createCounter('tview'),
                    edit: createCounter('tedit')
                }],
                validations: {
                    test: createCounter('validation', true)
                }
            };

            function createCounter(name, res) {
                return function (val) {
                    plugin['_' + name]++;
                    if (res === undefined) {
                        return arguments[arguments.length];
                    } else {
                        return res;
                    }
                }
            }

            // Add the plugin for access
            service.add('test', plugin);

            element = $compile('<div ng-model="data.value" ez-edit="test"></div>')($rootScope);
            $scope = element.scope();
            $scope.data = {
                value: 'foo bar'
            };
            $rootScope.$apply();

            // Reset the values after init
            plugin._view = 0;
            plugin._edit = 0;
            plugin._tview = 0;
            plugin._tedit = 0;
            plugin._validation = 0;
        });

        describe('requirements', function () {
            it('should require ngModel', function () {
                expect(comp).toThrowError(/ngmodel/i);

                /** Called to compile the invalid version */
                function comp() {
                    $compile('<div ez-edit="test"></div>')($rootScope);
                }
            });
            it('should print a warning when an invalid type is specified', function () {
                $compile('<div ng-model="data.value" ez-edit></div>')($rootScope);
                expect($log._warn).toBe(1);
            });
        });

        describe('transforms and validations', function () {
            it('should add the view part of the transforms as $formatters to ngModel', function () {
                expect($scope.$eze.ngModel.$formatters.length).toBe(1);
                expect($scope.$eze.ngModel.$formatters[0]).toEqual(jasmine.any(Function));
                $scope.$eze.ngModel.$formatters[0]('test');
                expect(plugin._tview).toBe(1);
            });
            it('should add the edit part of the transforms as $parsers to ngModel', function () {

                expect($scope.$eze.ngModel.$parsers.length).toBe(1);
                expect($scope.$eze.ngModel.$parsers[0]).toEqual(jasmine.any(Function));
                $scope.$eze.ngModel.$parsers[0]('test');
                expect(plugin._tedit).toBe(1);
            });
            it('should add the plugin validations to $validators to ngModel', function () {
                expect(Object.keys($scope.$eze.ngModel.$validators).length).toBe(1);
                expect($scope.$eze.ngModel.$validators.test).toEqual(jasmine.any(Function));
            });
            it('should add validations found in the ez-validate attribute to $validators to ngModel', function () {
                element = $compile('<div ng-model="data.value" ez-edit="test" ez-validate="required,maxLength[2]"></div>')($rootScope);
                $timeout.flush();
                $rootScope.$apply();
                $scope = element.scope();
                expect(Object.keys($scope.$eze.ngModel.$validators).length).toBe(3);
                expect($scope.$eze.ngModel.$validators.test).toEqual(jasmine.any(Function));
                expect($scope.$eze.ngModel.$validators.required).toEqual(jasmine.any(Function));
                expect($scope.$eze.ngModel.$validators.maxLength).toEqual(jasmine.any(Function));
            });
        });

        describe('plugin', function () {
            it('should call plugin.view on initial load when it exists', function () {
                element = $compile('<div ng-model="data.value" ez-edit="test"></div>')($rootScope);
                $rootScope.$apply();
                expect(plugin._view > 0).toBe(true);
            });
            it('should call element.text instead of plugin.view on initial load when it does not exist', function () {
                var called = false;
                delete plugin.view;
                angular.element.prototype.text = function () {
                    called = true;
                };
                $compile('<div ng-model="data.value" ez-edit="test"></div>')($rootScope);
                expect(called).toBe(true);
            });
        });

        describe('scope', function () {
            it('should emit the ez.edit.added event on initialization', function () {
                var called;
                $rootScope.$on('ez.edit.added', function () {
                    called = true;
                });
                $timeout.flush();
                $rootScope.$apply();
                $compile('<div ng-model="data.value" ez-edit="test"></div>')($rootScope);
                expect(called).toBe(true);
            });
            it('should emit the ez.edit.removed event on destruction', function () {
                var called;
                $rootScope.$on('ez.edit.removed', function () {
                    called = true;
                });
                element = $compile('<div ng-model="data.value" ez-edit="test"></div>')($rootScope);
                $scope = element.scope();
                $scope.$destroy();
                $timeout.flush();
                $rootScope.$apply();
                expect(called).toBe(true);
            });
            it('should emit the ez.edit.editing event when beginning to edit', function () {
                var called;
                $rootScope.$on('ez.edit.editing', function () {
                    called = true;
                });
                $scope.$eze.edit();
                $timeout.flush();
                $rootScope.$apply();
                expect(called).toBe(true);
            });
            it('should emit the ez.edit.viewing event when editing complete', function () {
                var called;
                $rootScope.$on('ez.edit.viewing', function () {
                    called = true;
                });
                $scope.$eze.edit();
                $timeout.flush();
                $rootScope.$apply();
                $scope.$eze.cancel();
                $rootScope.$apply();
                expect(called).toBe(true);
            });

            describe('$eze', function () {
                it('should exist', function () {
                    expect($scope.$eze).toEqual(jasmine.any(Object));
                });
                it('should contain a reference to the element', function () {
                    expect($scope.$eze.element[0]).toEqual(element[0]);
                });
                describe('editing', function () {
                    it('should exist', function () {
                        expect($scope.$eze.editing).toEqual(jasmine.any(Function));
                    });
                    it('should return whether the scope is in edit mode', function () {
                        expect($scope.$eze.editing()).toBe(false);
                        $scope.$eze.edit();
                        expect($scope.$eze.editing()).toBe(true);
                        $scope.$eze.cancel();
                        expect($scope.$eze.editing()).toBe(false);

                    });
                });
                describe('edit', function () {
                    it('should exist', function () {
                        expect($scope.$eze.edit).toEqual(jasmine.any(Function));
                    });
                    it('should call plugin.edit', function () {
                        expect(plugin._edit).toBe(0);
                        $scope.$eze.edit();
                        expect(plugin._edit).toBe(1);
                    });
                    it('should put the scope into edit mode', function() {
                        expect($scope.$eze.editing()).toBe(false);
                        $scope.$eze.edit();
                        expect($scope.$eze.editing()).toBe(true);
                    });
                });
                describe('cancel', function () {
                    it('should exist', function () {
                        expect($scope.$eze.cancel).toEqual(jasmine.any(Function));
                    });
                    it('should call plugin.view', function () {
                        $scope.$eze.edit();
                        expect(plugin._view).toBe(0);
                        $scope.$eze.cancel();
                        expect(plugin._view).toBe(1);
                    });
                    it('should put the scope into view mode', function () {
                        expect($scope.$eze.editing()).toBe(false);
                        $scope.$eze.edit();
                        expect($scope.$eze.editing()).toBe(true);
                        $scope.$eze.cancel();
                        expect($scope.$eze.editing()).toBe(false);
                    });
                });
                describe('save', function () {
                    it('should exist', function () {
                        expect($scope.$eze.save).toEqual(jasmine.any(Function));
                    });
                    it('should validate that the model value is correct', function () {
                        var res = false;
                        $scope.$eze.ngModel.$validators.testCheck = function() {
                            return res;
                        };
                        $scope.$eze.edit();
                        expect($scope.$eze.editing()).toBe(true);
                        expect($scope.$eze.save()).toBe(false);
                        expect($scope.$eze.editing()).toBe(true);
                        res = true;
                        expect($scope.$eze.save()).toBe(true);
                        expect($scope.$eze.editing()).toBe(false);
                    });
                    it('should call plugin.view', function () {
                        $scope.$eze.edit();
                        expect(plugin._view).toBe(0);
                        $scope.$eze.save();
                        expect(plugin._view).toBe(1);
                    });
                    it('should put the scope into view mode', function () {
                        $scope.$eze.edit();
                        expect($scope.$eze.editing()).toBe(true);
                        $scope.$eze.save();
                        expect($scope.$eze.editing()).toBe(false);
                    });
                });
                describe('reset', function () {
                    it('should exist', function () {
                        expect($scope.$eze.reset).toEqual(jasmine.any(Function));
                    });
                    it('should revert ngModel back to the value it was before entering edit mode', function () {
                        $scope.$eze.ngModel.$formatters = [];
                        $scope.$eze.ngModel.$parsers = [];

                        $scope.$eze.ngModel.$setViewValue('foo bar');
                        expect($scope.$eze.ngModel.$viewValue).toBe('foo bar');
                        $scope.$eze.edit();
                        $rootScope.$apply();

                        $scope.$eze.ngModel.$setViewValue('baz');
                        expect($scope.$eze.ngModel.$viewValue).toBe('baz');
                        $scope.$eze.reset();
                        $rootScope.$apply();

                        expect($scope.$eze.ngModel.$viewValue).toBe('foo bar');
                    });
                });
                describe('valid', function () {
                    it('should return whether the entry is valid or not', function () {
                        var res = false;
                        $scope.$eze.ngModel.$validators.test = function () {
                            return res;
                        };
                        expect($scope.$eze.valid()).toBe(false);
                        res = true;
                        expect($scope.$eze.valid()).toBe(true);
                    });
                });
                describe('errors', function () {
                    it('should exist', function () {
                        expect($scope.$eze.errors).toEqual(jasmine.any(Function));
                    });
                    it('should return an object containing any validation errors which may have occured', function () {
                        $scope.$eze.ngModel.$validators.foo = function(val) {
                            return false;
                        };
                        $scope.$eze.edit();
                        $scope.$eze.save();
                        expect($scope.$eze.errors().foo).toBe(true);
                    });
                });
            });
        });

        describe('attributes', function () {
            it('should execute the expression found in onsave when saving', function () {
                var called = false;
                $rootScope.testSave = function (val) {
                    expect(val).toBe('foo');
                    called = true;
                };
                element = $compile('<div ng-model="data.value" ez-edit="test" onsave="testSave(\'foo\')"></div>')($rootScope);
                $scope = element.scope();
                $scope.$eze.edit();
                $scope.$eze.save();
                $timeout.flush();
                $rootScope.$apply();
                expect(called).toBe(true);
            });
            it('should execute the expression found in onedit when editing', function () {
                var called = false;
                $rootScope.test = function (val) {
                    expect(val).toBe('foo');
                    called = true;
                };
                element = $compile('<div ng-model="data.value" ez-edit="test" onedit="test(\'foo\')"></div>')($rootScope);
                $scope = element.scope();
                $scope.$eze.edit();
                $timeout.flush();
                $rootScope.$apply();
                expect(called).toBe(true);
            });
            it('should execute the expression found in onview when editing has ended', function () {
                var called = false;
                $rootScope.test = function (val) {
                    expect(val).toBe('foo');
                    called = true;
                };
                element = $compile('<div ng-model="data.value" ez-edit="test" onview="test(\'foo\')"></div>')($rootScope);
                $scope = element.scope();
                $scope.$eze.edit();
                $scope.$eze.save();
                $timeout.flush();
                $rootScope.$apply();
                expect(called).toBe(true);
            });
            it('should execute the expression found in onerror when unable to save', function () {
                var called = false;
                $rootScope.test = function (val) {
                    expect(val).toBe('foo');
                    called = true;
                };
                element = $compile('<div ng-model="data.value" ez-edit="test" onerror="test(\'foo\')"></div>')($rootScope);
                $scope = element.scope();
                $scope.$eze.ngModel.$validators.check = function () { return false; }
                $scope.$eze.edit();
                $scope.$eze.save();
                $timeout.flush();
                $rootScope.$apply();
                expect(called).toBe(true);
            });
            it('should execute the expression found in oncancel when cancelled', function () {
                var called = false;
                $rootScope.test = function (val) {
                    expect(val).toBe('foo');
                    called = true;
                };
                element = $compile('<div ng-model="data.value" ez-edit="test" oncancel="test(\'foo\')"></div>')($rootScope);
                $scope = element.scope();
                $scope.$eze.edit();
                $scope.$eze.cancel();
                $timeout.flush();
                $rootScope.$apply();
                expect(called).toBe(true);
            });
            it('should execute the expression found in onreset when reset', function () {
                var called = false;
                $rootScope.test = function (val) {
                    expect(val).toBe('foo');
                    called = true;
                };
                element = $compile('<div ng-model="data.value" ez-edit="test" onreset="test(\'foo\')"></div>')($rootScope);
                $scope = element.scope();
                $scope.$eze.edit();
                $scope.$eze.reset();
                $timeout.flush();
                $rootScope.$apply();
                expect(called).toBe(true);
            });
        });

    });
}());
