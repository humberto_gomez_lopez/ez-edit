(function editServiceTests() {

    describe('edit service', function () {
        var service, transformService;
        beforeEach(inject(['ezEdit', 'ezEditTransform', function (edit, transform) {
            service = edit;
            transformService = transform;
        }]));
        describe('add', function () {
            it('should exist', function () {
                expect(service.add).toEqual(jasmine.any(Function));
            });
            it('should ensure type is a string', function () {
                var plugin = { edit: function () { } };
                expect(service.add.bind(service, 1, plugin))
                    .toThrowError(/type.*string/i);
            });
            it('should validate the added plugin', function () {
                var plugin;
                expect(service.add.bind(service, 'test', plugin))
                    .toThrowError(/object/i);
                plugin = {};
                expect(service.add.bind(service, 'test', plugin))
                    .toThrowError(/edit/i);
                plugin.edit = function () { };
                plugin.view = 'foo bar';
                expect(service.add.bind(service, 'test', plugin))
                    .toThrowError(/view.*function/i);
                delete plugin.view;
                plugin.transforms = 'foo bar';
                expect(service.add.bind(service, 'test', plugin))
                    .toThrowError(/transforms.*array/i);

                delete plugin.transforms;
                plugin.validations = 'foo bar';
                expect(service.add.bind(service, 'test', plugin))
                    .toThrowError(/validations.*object/i);
            });
            it('should add the plugin', function () {
                var plugin = {
                    edit: function () { }
                };
                service.add('test', plugin);
                expect(service.plugin('test')).toBe(plugin);
            });
            it('should load any transforms defined as a name in the transforms array automatically', function () {
                var plugin = {
                    edit: function () { },
                    transforms: ['tt', 1]
                };
                transformService.add('tt', noop, noop);
                service.add('test', plugin);
                expect(plugin.transforms[0]).toEqual(jasmine.any(Object));
                expect(plugin.transforms[1]).toBe(1);

                function noop() { }
            });
        });
        describe('remove', function () {
            it('should exist', function () {
                expect(service.remove).toEqual(jasmine.any(Function));
            });
            it('should return the removed plugin', function () {
                var plugin = {
                    edit: function () { }
                };
                service.add('test', plugin);
                expect(service.plugin('test')).toBe(plugin);
                expect(service.remove('test')).toBe(plugin);
                expect(service.remove('test')).toBe(undefined);
            });
        });
        describe('list', function () {
            it('should exist', function () {
                expect(service.list).toEqual(jasmine.any(Function));
            });
            it('should return an array of plugin names', function () {
                var list, plugin = {
                    edit: function () { }
                };
                service.add('p1', plugin);
                service.add('p2', plugin);
                list = service.list();
                expect(list).toEqual(jasmine.any(Array));
                expect(list.length).toBe(2);
                expect(list[0]).toBe('p1');
                expect(list[1]).toBe('p2');
            });
        });
        describe('plugin', function () {
            it('should exist', function () {
                expect(service.plugin).toEqual(jasmine.any(Function));
            });
            it('should return the named plugin', function () {
                var p1 = { edit: noop },
                    p2 = { edit: noop };
                service.add('p1', p1);
                service.add('p2', p2);
                expect(service.plugin('p1')).toBe(p1);
                expect(service.plugin('p2')).toBe(p2);
                function noop() { }
            });
            it('should throw an error if the specified type is not found', function () {
                expect(service.plugin.bind(service, 'foo bar'))
                    .toThrowError(/plugin.*named/i);
            });
        });
    });

}());
