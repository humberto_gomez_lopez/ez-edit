(function transformServiceSpec() {
    'use strict';

    describe('transform service', function () {
        var service;
        beforeEach(inject(['ezEditTransform', function (editTransform) {
            service = editTransform;
        }]));

        describe('$get()', function () {
            it('should exist', function () {
                expect(service.$get).toEqual(jasmine.any(Function));
            });
            it('should return the service', function () {
                expect(service.$get()).toBe(service);
            });
        });
        describe('add(name, view, edit)', function () {
            it('should exist', function () {
                expect(service.add).toEqual(jasmine.any(Function));
            });
            it('should ensure name is a string', function () {
                expect(service.add.bind(service, 1, noop, noop))
                    .toThrowError(/name.*string/);
            });
            it('should ensure view is a function', function () {
                expect(service.add.bind(service, 'foo', 'bar', noop))
                    .toThrowError(/view.*function/);
                expect(service.add.bind(service, 'foo', undefined, noop))
                    .toThrowError(/view.*function/);
            });
            it('should ensure edit is a function', function () {
                expect(service.add.bind(service, 'foo', noop, 'bar'))
                    .toThrowError(/edit.*function/);
                expect(service.add.bind(service, 'foo', noop, undefined))
                    .toThrowError(/edit.*function/);
            });
            it('should add the named transform', function () {
                var tfm;
                service.add('test', view, edit);
                tfm = service.transform('test');
                expect(tfm.view).toBe(view);
                expect(tfm.edit).toBe(edit);
                function view() { }
                function edit() { }
            });
        });
        describe('remove(name)', function () {
            it('should exist', function () {
                expect(service.remove).toEqual(jasmine.any(Function));
            });
            it('should remove the named transform', function () {
                service.add('test', noop, noop);
                expect(service.transform('test')).toEqual(jasmine.any(Object));
                service.remove('test');
                expect(service.transform('test')).toBe(undefined);
            });
            it('should return the removed transform', function () {
                var tfm;
                service.add('test', noop, noop);
                tfm = service.transform('test');
                expect(service.remove('test')).toBe(tfm);
            });
        });
        describe('transformView(name, element, scope, value)', function () {
            it('should exist', function () {
                expect(service.transformView).toEqual(jasmine.any(Function));
            });
            it('should throw an error if the transform does not exist', function () {
                expect(service.transformView.bind(service, 'foo bar'))
                    .toThrowError(/exist/);
            });
            it('should apply the named view transform', function () {
                var ele = { },
                    scp = { },
                    val = 10;

                service.add('test', transform, noop);
                expect(service.transformView('test', ele, scp, val)).toBe(20);

                function transform(element, scope, value) {
                    expect(element).toBe(ele);
                    expect(scope).toBe(scp);
                    return value + 10;
                }
            });
        });
        describe('transformEdit(name, element, scope, value)', function () {
            it('should exist', function () {
                expect(service.transformEdit).toEqual(jasmine.any(Function));
            });
            it('should throw an error if the transform does not exist', function () {
                expect(service.transformEdit.bind(service, 'foo bar'))
                    .toThrowError(/exist/);
            });
            it('should apply the named edit transform', function () {
                var ele = { },
                    scp = { },
                    val = 10;

                service.add('test', noop, transform);
                expect(service.transformEdit('test', ele, scp, val)).toBe(20);

                function transform(element, scope, value) {
                    expect(element).toBe(ele);
                    expect(scope).toBe(scp);
                    return value + 10;
                }
            });
        });
        describe('transform(name)', function () {
            it('should exist', function () {
                expect(service.transform).toEqual(jasmine.any(Function));
            });
            it('return an object containing the view and edit functions for the named transform', function () {
                var tfm;
                service.add('test', view, edit);
                tfm = service.transform('test');
                expect(tfm).toEqual(jasmine.any(Object));
                expect(tfm.view).toBe(view);
                expect(tfm.edit).toBe(edit);
                function view() { }
                function edit() { }
            });
        });
    });

    function noop() { }
}());
