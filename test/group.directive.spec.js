(function groupDirectiveTests() {

    describe('ezEditGroup', function () {
        var $provide, $compile, $timeout, $rootScope, $scope, service,
            element, plugin, $log;

        beforeEach(module(['$provide', function (provide) {
            $provide = provide;
        }]));

        // Inject everything we need
        beforeEach(inject(['$compile', '$rootScope', '$timeout', 'ezEdit', function (compile, rootScope, timeout, edit) {
            $compile = compile;
            $rootScope = rootScope;
            $timeout = timeout;
            service = edit;

            $log = {
                _warn: 0,
                warn: function () {
                    this._warn++;
                }
            };
            $provide.value('$log', $log);
        }]));

        // Prepare the element
        beforeEach(function () {
            plugin = {
                _view: 0,
                _edit: 0,
                view: createCounter('view'),
                edit: createCounter('edit'),
                transforms: [],
                validations: {}
            };

            function createCounter(name, res) {
                return function (val) {
                    plugin['_' + name]++;
                    if (res === undefined) {
                        return arguments[arguments.length];
                    } else {
                        return res;
                    }
                }
            }

            // Add the plugin for access
            service.add('test', plugin);

            element = $compile('<div ez-edit-group><div ng-model="data.value1" ez-edit="test"></div><div ng-model="data.value2" ez-edit="test"></div></div>')($rootScope);
            $scope = element.scope();
            $scope.data = {
                value1: 'foo',
                value2: 'bar'
            };
            $rootScope.$apply();
            $timeout.flush();

            // Reset the values after init
            plugin._view = 0;
            plugin._edit = 0;
        });

        describe('scope', function () {
            it('should emit the ez.edit.added event on initialization', function () {
                var called = false;
                $rootScope.$on('ez.edit.added', function () {
                    called = true;
                });
                element = $compile('<div><div ng-model="data.value1" ez-edit="test"></div><div ng-model="data.value2" ez-edit="test"></div></div>')($rootScope);
                $timeout.flush();
                $rootScope.$apply();
                expect(called).toBe(true);
            });
            it('should emit the ez.edit.removed event on destruction', function () {
                var called = false;
                $rootScope.$on('ez.edit.removed', function () {
                    called = true;
                });
                element = $compile('<div><div ng-model="data.value1" ez-edit="test"></div><div ng-model="data.value2" ez-edit="test"></div></div>')($rootScope);
                $scope = element.scope();
                $scope.$destroy();
                $timeout.flush();
                $rootScope.$apply();
                expect(called).toBe(true);
            });
            it('should emit the ez.edit.editing event when beginning to edit', function () {
                var called = false;
                $rootScope.$on('ez.edit.editing', function () {
                    called = true;
                });
                $scope.$eze.edit();
                $rootScope.$apply();
                expect(called).toBe(true);
            });
            it('should emit the ez.edit.viewing event when editing complete', function () {
                var called;
                $rootScope.$on('ez.edit.viewing', function () {
                    called = true;
                });
                $scope.$eze.edit();
                $rootScope.$apply();
                $scope.$eze.cancel();
                $rootScope.$apply();
                expect(called).toBe(true);
            });

            describe('$eze', function () {
                it('should exist', function () {
                    expect($scope.$eze).toEqual(jasmine.any(Object));
                });
                it('should contain a reference to the element', function () {
                    expect($scope.$eze.element[0]).toEqual(element[0]);
                });
                describe('editing', function () {
                    it('should exist', function () {
                        expect($scope.$eze.editing).toEqual(jasmine.any(Function));
                    });
                    it('should return true when all child controls are editing', function () {
                        var d1 = angular.element(element.children()[0]),
                            d2 = angular.element(element.children()[1]),
                            res1 = false,
                            res2 = false,
                            $sc1 = d1.scope(),
                            $sc2 = d2.scope();

                        expect($scope.$eze.editing()).toBe(false);
                        $sc1.$eze.edit();
                        expect($scope.$eze.editing()).not.toBe(true);
                        $sc2.$eze.edit();
                        expect($scope.$eze.editing()).toBe(true);
                    });
                    it('should return undefined when some child controls are editing', function () {
                        var d1 = angular.element(element.children()[0]),
                            d2 = angular.element(element.children()[1]),
                            res1 = false,
                            res2 = false,
                            $sc1 = d1.scope(),
                            $sc2 = d2.scope();

                        expect($scope.$eze.editing()).toBe(false);
                        $sc1.$eze.edit();
                        expect($scope.$eze.editing()).toBe(undefined);
                    });
                    it('should return false when no child controls are editing', function () {
                        var d1 = angular.element(element.children()[0]),
                            d2 = angular.element(element.children()[1]),
                            res1 = false,
                            res2 = false,
                            $sc1 = d1.scope(),
                            $sc2 = d2.scope();

                        expect($scope.$eze.editing()).toBe(false);
                        $sc1.$eze.edit();
                        expect($scope.$eze.editing()).toBe(undefined);
                        $sc1.$eze.cancel();
                        expect($scope.$eze.editing()).toBe(false);
                    });
                });
                describe('edit', function () {
                    it('should exist', function () {
                        expect($scope.$eze.edit).toEqual(jasmine.any(Function));
                    });
                    it('should call edit on all child controls', function () {
                        var d1 = angular.element(element.children()[0]),
                            d2 = angular.element(element.children()[1]),
                            res1 = false,
                            res2 = false,
                            $sc1 = d1.scope(),
                            $sc2 = d2.scope(),
                            c1 = false,
                            c2 = false;
                        $sc1.$eze.edit = function () { c1 = true; };
                        $sc2.$eze.edit = function () { c2 = true; };

                        $scope.$eze.edit();
                        expect(c1).toBe(true);
                        expect(c2).toBe(true);
                    });
                });
                describe('cancel', function () {
                    it('should exist', function () {
                        expect($scope.$eze.cancel).toEqual(jasmine.any(Function));
                    });
                    it('should call cancel on all child controls', function () {
                        var d1 = angular.element(element.children()[0]),
                            d2 = angular.element(element.children()[1]),
                            res1 = false,
                            res2 = false,
                            $sc1 = d1.scope(),
                            $sc2 = d2.scope(),
                            c1 = false,
                            c2 = false;
                        $sc1.$eze.cancel = function () { c1 = true; };
                        $sc2.$eze.cancel = function () { c2 = true; };

                        $scope.$eze.cancel();
                        expect(c1).toBe(true);
                        expect(c2).toBe(true);
                    });
                });
                describe('save', function () {
                    it('should exist', function () {
                        expect($scope.$eze.save).toEqual(jasmine.any(Function));
                    });
                    it('should validate all child controls', function () {
                        var d1 = angular.element(element.children()[0]),
                            d2 = angular.element(element.children()[1]),
                            res1 = false,
                            res2 = false,
                            $sc1 = d1.scope(),
                            $sc2 = d2.scope(),
                            c1 = false,
                            c2 = false;
                        $sc1.$eze.valid = function () { c1 = true; };
                        $sc2.$eze.valid = function () { c2 = true; };

                        $scope.$eze.save();
                        expect(c1).toBe(true);
                        expect(c2).toBe(true);
                    });
                    it('should not call save on any child control when any of the child controls is invalid', function () {
                        var d1 = angular.element(element.children()[0]),
                            d2 = angular.element(element.children()[1]),
                            res1 = false,
                            res2 = false,
                            $sc1 = d1.scope(),
                            $sc2 = d2.scope(),
                            c1 = false,
                            c2 = false;
                        $sc2.$eze.valid = function () { return false };
                        $sc1.$eze.save = function () { c1 = true; };
                        $sc2.$eze.save = function () { c2 = true; };

                        $scope.$eze.save();
                        expect(c1).toBe(false);
                        expect(c2).toBe(false);
                    });
                    it('should call save on all child controls', function () {
                        var d1 = angular.element(element.children()[0]),
                            d2 = angular.element(element.children()[1]),
                            res1 = false,
                            res2 = false,
                            $sc1 = d1.scope(),
                            $sc2 = d2.scope(),
                            c1 = false,
                            c2 = false;
                        $sc1.$eze.save = function () { c1 = true; };
                        $sc2.$eze.save = function () { c2 = true; };

                        $scope.$eze.save();
                        expect(c1).toBe(true);
                        expect(c2).toBe(true);
                    });
                });
                describe('reset', function () {
                    it('should exist', function () {
                        expect($scope.$eze.reset).toEqual(jasmine.any(Function));
                    });
                    it('should call reset on all child controls', function () {
                        var d1 = angular.element(element.children()[0]),
                            d2 = angular.element(element.children()[1]),
                            res1 = false,
                            res2 = false,
                            $sc1 = d1.scope(),
                            $sc2 = d2.scope(),
                            c1 = false,
                            c2 = false;
                        $sc1.$eze.reset = function () { c1 = true; };
                        $sc2.$eze.reset = function () { c2 = true; };

                        $scope.$eze.reset();
                        expect(c1).toBe(true);
                        expect(c2).toBe(true);
                    });
                });
                describe('valid', function () {
                    it('should return whether all entries are valid or not', function () {
                        var d1 = angular.element(element.children()[0]),
                            d2 = angular.element(element.children()[1]),
                            res1 = false,
                            res2 = false,
                            $sc1 = d1.scope(),
                            $sc2 = d2.scope();
                        $sc1.$eze.ngModel.$validators.foo = function(val) {
                            return res1;
                        };
                        $sc2.$eze.ngModel.$validators.foo = function(val) {
                            return res2;
                        };
                        expect($scope.$eze.valid()).toBe(false);
                        res1 = true;
                        expect($scope.$eze.valid()).toBe(false);
                        res2 = true;
                        expect($scope.$eze.valid()).toBe(true);
                    });
                });
                describe('errors', function () {
                    it('should exist', function () {
                        expect($scope.$eze.errors).toEqual(jasmine.any(Function));
                    });
                    it('should return an array of objects containing any validation errors which may have occured', function () {
                        var d1 = angular.element(element.children()[0]),
                            d2 = angular.element(element.children()[1]),
                            $sc1 = d1.scope(),
                            $sc2 = d2.scope();
                        $sc1.$eze.ngModel.$validators.foo = function(val) {
                            return false;
                        };
                        $sc2.$eze.ngModel.$validators.foo = function(val) {
                            return false;
                        };
                        $scope.$eze.edit();
                        $scope.$eze.save();
                        expect($scope.$eze.errors()[0].errors.foo).toBe(true);
                        expect($scope.$eze.errors()[1].errors.foo).toBe(true);
                    });
                });
            });
        });

        describe('attributes', function () {
            it('should execute the expression found in onsave when saving', function () {
                var called = false;
                $rootScope.test = function (val) {
                    expect(val).toBe('foo');
                    called = true;
                };
                element = $compile('<div ez-edit-group onsave="test(\'foo\')"><div ng-model="data.value" ez-edit="test"></div></div>')($rootScope);
                $scope = element.scope();
                $timeout.flush();
                $rootScope.$apply();
                $scope.$eze.edit();
                $scope.$eze.save();
                $timeout.flush();
                $rootScope.$apply();
                expect(called).toBe(true);
            });
            it('should execute the expression found in onedit when editing', function () {
                var called = false;
                $rootScope.test = function (val) {
                    expect(val).toBe('foo');
                    called = true;
                };
                element = $compile('<div ez-edit-group onedit="test(\'foo\')"><div ng-model="data.value" ez-edit="test"></div></div>')($rootScope);
                $scope = element.scope();
                $timeout.flush();
                $rootScope.$apply();
                $scope.$eze.edit();
                $timeout.flush();
                $rootScope.$apply();
                expect(called).toBe(true);
            });
            it('should execute the expression found in onview when editing has ended', function () {
                var called = false;
                $rootScope.test = function (val) {
                    expect(val).toBe('foo');
                    called = true;
                };
                element = $compile('<div ez-edit-group onview="test(\'foo\')"><div ng-model="data.value" ez-edit="test"></div></div>')($rootScope);
                $scope = element.scope();
                $timeout.flush();
                $rootScope.$apply();
                $scope.$eze.edit();
                $scope.$eze.save();
                $timeout.flush();
                $rootScope.$apply();
                expect(called).toBe(true);
            });
            it('should execute the expression found in oncancel when cancelled', function () {
                var called = false;
                $rootScope.test = function (val) {
                    expect(val).toBe('foo');
                    called = true;
                };
                element = $compile('<div ez-edit-group oncancel="test(\'foo\')"><div ng-model="data.value" ez-edit="test"></div></div>')($rootScope);
                $scope = element.scope();
                $timeout.flush();
                $rootScope.$apply();
                $scope.$eze.edit();
                $scope.$eze.cancel();
                $timeout.flush();
                $rootScope.$apply();
                expect(called).toBe(true);
            });
            it('should execute the expression found in onreset when reset', function () {
                var called = false;
                $rootScope.test = function (val) {
                    expect(val).toBe('foo');
                    called = true;
                };
                element = $compile('<div ez-edit-group onreset="test(\'foo\')"><div ng-model="data.value" ez-edit="test"></div></div>')($rootScope);
                $scope = element.scope();
                $timeout.flush();
                $rootScope.$apply();
                $scope.$eze.edit();
                $scope.$eze.reset();
                $timeout.flush();
                $rootScope.$apply();
                expect(called).toBe(true);
            });
        });
    });

}());
