// karma.conf.js
module.exports = function(config) {
    config.set({
        basePath: '',
        frameworks: ['jasmine'],
        files: [
            'bower_components/angular/angular.min.js',
            'bower_components/angular-mocks/angular-mocks.js',

            'src/module.js',
            'src/service/transform.service.js',
            'src/service/validation.service.js',
            'src/service/edit.service.js',
            'src/directive/edit.js',
            'src/directive/group.js',
            'src/plugin/transform.js',
            'src/plugin/validation.js',

            'test/**/support.js',
            'test/**/*.spec.js'
        ],
        preprocessors: {
            'src/**/*.js': ['coverage']
        },
        reporters: [
            'dots',
            'coverage'
        ],
        coverageReporter: {
            type : 'html',
            dir : 'test/coverage'
        },
        logLevel: config.LOG_INFO,
        colors: true,
        browsers: ['Chrome'],
        singleRun: true
    });
};
